
public class Calculadora {
	private int numA;
	private int numB;
	private char operador;
	
	public Calculadora(int num1,int num2) {
		this.numA=num1;
		this.numB=num2;
	}

	public Calculadora() {
		this.numA=0;
		this.numB=0;
		
	}
	
	public void cargaString(String cad) {
		String num1,num2;
		int posOperacion=-1;
		if(cad.indexOf('+')!=-1) {
			posOperacion=cad.indexOf('+');
		}
		if(cad.indexOf('-')!=-1) {
			posOperacion=cad.indexOf('-');
		}
		if(cad.indexOf('*')!=-1) {
			posOperacion=cad.indexOf('*');
		}
		if(cad.indexOf('/')!=-1) {
			posOperacion=cad.indexOf('/');
		}
		
		if(posOperacion!=-1) {
			num1=cad.substring(0,posOperacion);
			num2=cad.substring(posOperacion+1,cad.length());
			
			this.numA=Integer.parseInt(num1);
			this.numB=Integer.parseInt(num2);
			this.operador=cad.charAt(posOperacion);
		}
		else
			this.operador='0';
	}
	
	
	
	public int sumar(){
		
		return this.numA+this.numB;
	}
	
	public int multiplicar() {
		return this.numA*this.numB;
	}
	
	
	public int restar(){
		return this.numA-this.numB;
	}
	
	public int dividir(){
		return this.numA/this.numB;
	}
	
	
	public int getNumB() {
		return numB;
	}

	public void setNumB(int numB) {
		this.numB = numB;
	}


	public int getExpresion() {
		return numA;
	}

	public void setExpresion(int expresion) {
		this.numA = expresion;
	}

	public char getOperador() {
		return operador;
	}

	public void setOperador(char operador) {
		this.operador = operador;
	}

	public int getNumA() {
		return numA;
	}

	public void setNumA(int numA) {
		this.numA = numA;
	}
	
	
	
	
	
	
	
}

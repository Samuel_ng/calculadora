
import javax.swing.*;
import java.awt.event.*;

import java.awt.*;

public class CalculadoraGUI extends JFrame implements ActionListener {
	
	
	JLabel etiqueta;
	JPanel panelArriba,panelAbajo;
	Container contenedor;
	JButton[] numeros;
	JButton[] operaciones;
	
	public CalculadoraGUI() {
		super("calculadora");//nombre de la ventana
		this.contenedor=this.getContentPane();
		this.setSize(400, 300);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		contenedor.setLayout(new GridLayout(2,1));
		
		prepararPanelArriba();
		prepararPanelAbajo();
		
		
		contenedor.add(panelArriba);
		contenedor.add(panelAbajo);
		
	}
	
	private void prepararPanelArriba() {
		panelArriba= new JPanel();
		etiqueta= new JLabel();
		etiqueta.setText("");
		panelArriba.add(etiqueta);
		//panelArriba.setBackground(Color.BLACK);
		
	}
	private void prepararPanelAbajo() {
		panelAbajo= new JPanel();
		//panelAbajo.setBackground(Color.GREEN);
		panelAbajo.setLayout(new GridLayout(1,2));//GridLayout(1,2)
		JPanel Izquierdo,Derecho;
		
		
		
		Izquierdo=new JPanel();
		Izquierdo.setLayout(new GridLayout(4,3));//FlowLayout()
		
		numeros=new JButton[10];
		for(int i=0;i<numeros.length;i++) {
			numeros[i]=new JButton(""+i);
			numeros[i].addActionListener(this);
			Izquierdo.add(numeros[i]);
			
		}
		/*
		URL direccion =this.getClass().getResource("/imagenes/0.png");
		ImageIcon icono= new ImageIcon(direccion);
		numeros[0].setIcon(icono);*/
		
		Derecho=new JPanel();
		Derecho.setLayout(new GridLayout(2,4));
		operaciones=new JButton[6];
		for(int i=0;i<operaciones.length;i++) {
			operaciones[i]=new JButton();
			operaciones[i].addActionListener(this);
			Derecho.add(operaciones[i]);
		}
		operaciones[0].setText("+");
		operaciones[1].setText("-");
		operaciones[2].setText("*");
		operaciones[3].setText("/");
		operaciones[4].setText("=");
		operaciones[5].setText("C");
		
		panelAbajo.add(Izquierdo);
		panelAbajo.add(Derecho);
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton botonSeleccionado=new JButton();
		botonSeleccionado=(JButton)e.getSource();//solo por que todos los elementos activos de la interface son botones
		
		String seleccion=botonSeleccionado.getText();
		String expresion="";
		
		if(seleccion!="=") {
			this.mostrarNumeros(seleccion);
			this.mostrarOperaciones(seleccion);
		}
		else {
			if(StringEsValido(etiqueta.getText())) {
				
				Calculadora calculadora= new Calculadora();
				calculadora.cargaString(etiqueta.getText());
				
				if(calculadora.getOperador()!='0') {
					switch(calculadora.getOperador()) {
					case'+':
						etiqueta.setText(""+calculadora.sumar());
						break;
					case'-':
						etiqueta.setText(""+calculadora.restar());
						break;
					case'*':
						etiqueta.setText(""+calculadora.multiplicar());
						break;
					case'/':
						etiqueta.setText(""+calculadora.dividir());
						break;
					}
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "ERROR ingrese solo una operacion");
				etiqueta.setText("");
				
			}
				
			
			
		}
		
		
		
		
	}
	
	private boolean StringEsValido(String cadena) {
		char caracterInicio;
		char caracterFinal;
		char caracter;
		int cont=0;
		if(!cadena.isEmpty()) {
			caracterInicio= cadena.charAt(0);
			caracterFinal= cadena.charAt(cadena.length()-1);
			
			if(caracterInicio!='+'&&caracterInicio!='-'&&caracterInicio!='*'&&caracterInicio!='/'&&caracterInicio!='=') {
				if(caracterFinal!='+'&&caracterFinal!='-'&&caracterFinal!='*'&&caracterFinal!='/'&&caracterInicio!='=') {
					for(int i=0;i<cadena.length();i++) {
						caracter=cadena.charAt(i);
						if(caracter=='+'||caracter=='-'||caracter=='*'||caracter=='/'||caracter=='=') {
							cont=cont+1;
						}
					}
					if(cont<=1)
						return true;
					else
						return false;
					
				}
				else
					return false;	
			}
			else
				return false;
		}
		else return false;
	}
	
	private void mostrarNumeros(String seleccion) {
		String expresion="";
		switch(seleccion) {
		case "0":
			expresion=numeros[0].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "1":
			expresion=numeros[1].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "2":
			expresion=numeros[2].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "3":
			expresion=numeros[3].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "4":
			expresion=numeros[4].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "5":
			expresion=numeros[5].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "6":
			expresion=numeros[6].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "7":
			expresion=numeros[7].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "8":
			expresion=numeros[8].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "9":
			expresion=numeros[9].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		}
		
	}
	
	private void mostrarOperaciones(String seleccion) {
		String expresion="";
		switch(seleccion) {
		case "+":
			expresion=operaciones[0].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "-":
			expresion=operaciones[1].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "*":
			expresion=operaciones[2].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "/":
			expresion=operaciones[3].getText();
			etiqueta.setText(etiqueta.getText()+ expresion);
			break;
		case "C":
			etiqueta.setText("");
			break;
		}
		}
}
